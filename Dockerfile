# Use a imagem oficial do Apache
FROM httpd:latest

# Copie o conteúdo do seu aplicativo para o diretório de trabalho do Apache
COPY ./apache-docker /usr/local/apache2/htdocs/

# Exponha a porta para acessar o Apache
EXPOSE 8088
